//
//  ViewController.m
//  Journal
//
//  Created by Randy Jorgensen on 2/8/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [myTableView reloadData];
    [self.view addSubview:myTableView];
    
    NSDate* DateToDisplay = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *theDate = [dateFormat stringFromDate:DateToDisplay];
    
    Entries = [NSMutableDictionary
     dictionaryWithDictionary:@{@"Start Here" : theDate}];
    
    
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:array forKey:@"Journal"];
//    [defaults setObject:array1 forKey:@"Date"];
//    [defaults synchronize];
//     array1 = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"Date"]];
    
    
    
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"+ New Journal Entry" forState:UIControlStateNormal];

    CGFloat buttonwidth = 250;
    CGFloat buttonheight = 57.5;
    button.frame = CGRectMake(0, self.view.bounds.size.height -150, buttonwidth, buttonheight);
    button.center = CGPointMake(self.view.center.x, self.view.bounds.size.height -155);
    button.titleLabel.font = [UIFont systemFontOfSize: 24];
    [button addTarget:self action:@selector(btntouched:)
     forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return Date.count;
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *CellIdentifier = @"cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
//    
//    cell.textLabel.text = [array1 objectAtIndex:indexPath.row];
//    return cell;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
   
    NSMutableDictionary* dictionary = Date[indexPath.row];
    NSDate* DateToDisplay = dictionary[@"Date"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  
    NSString *theDate = [dateFormat stringFromDate:DateToDisplay];
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
    
    
    
    cell.textLabel.text = theDate;
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    NSMutableDictionary* dictionary = Date[indexPath.row];
    
    DetailViewController* dvc = [DetailViewController new];
    dvc.infoDictionary = dictionary;
    [self.navigationController pushViewController:dvc animated:YES];
}
-(void)btntouched:(UIButton*)button {
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSArray *arrayDates = [userDefaults objectForKey:@"tableViewDataImage"];
//    NSArray *arrayJournal = [userDefaults objectForKey:@"tableViewDataText"];
   DetailViewController* dvc = [DetailViewController new];
   [self.navigationController pushViewController:dvc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
