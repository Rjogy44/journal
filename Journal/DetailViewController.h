//
//  DetailViewController.h
//  Journal
//
//  Created by Randy Jorgensen on 2/8/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController<UITextViewDelegate>
{
    UIView* sv;
    UITextView* label3;
    NSMutableArray* array;
   
    
}

@property (nonatomic) UITapGestureRecognizer* _tapRecognizer;

@property (nonatomic, strong) NSDictionary* infoDictionary;


@end
