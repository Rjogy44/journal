//
//  DetailViewController.m
//  Journal
//
//  Created by Randy Jorgensen on 2/8/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//


#import "DetailViewController.h"

@interface DetailViewController()

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor blueColor];
    [self.view addSubview:sv];
    
    
    
    UILabel* ig = [[UILabel alloc]initWithFrame:CGRectMake(125, 50, 125, 50)];
    ig.text = @"My Journal....";
    [ig setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    ig.contentMode = UIViewContentModeScaleAspectFit;
    [sv addSubview:ig];
 

    label3 = [[UITextView alloc]initWithFrame:CGRectZero];
    label3.text = self.infoDictionary[@"Journal"];
    label3.delegate = self;
    label3.contentMode = UIViewContentModeScaleAspectFit;
    label3.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label3];
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Save" forState:UIControlStateNormal];
    button.frame = CGRectZero;
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button addTarget:self action:@selector(btntouched:)
     forControlEvents:UIControlEventTouchUpInside];
    [sv addSubview:button];
    
    __tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    __tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:__tapRecognizer];
    
    
    
    
    
    
    
    
    
    
    
    NSDictionary* variableDictionary = NSDictionaryOfVariableBindings(label3,button);
    
    NSDictionary* metrics = nil;
    
    //
    //    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[ti]-|" options:0 metrics:metrics views:variableDictionary]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[ig]-|" options:0 metrics:metrics views:variableDictionary]];
    //    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[di]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label3]-|" options:0 metrics:metrics views:variableDictionary]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|" options:0 metrics:metrics views:variableDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-150-[label3]-[button]-|" options:0 metrics:metrics views:variableDictionary]];
    
    
    
    
    
    
    
}



- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(void)btntouched:(UIButton*)button
{
    [label3 resignFirstResponder] ;
    NSString* labelContents = label3.text ;
    NSLog(@"%@", labelContents);
    
    NSDate* DateToDisplay = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *theDate = [dateFormat stringFromDate:DateToDisplay];

    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults] ;
    [userDefaults setObject:labelContents forKey:@"Journal"];
    [userDefaults objectForKey:@"Journal"];
    [userDefaults setObject: theDate forKey:@"Date"];
    [userDefaults objectForKey:@"Date"];
    [userDefaults synchronize];
//    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
    
   
    [self.navigationController popToRootViewControllerAnimated:YES];
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
